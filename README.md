# UKCloudTest

Issues encountered during password generator test:

I wasn't sure how strictly to take the requirement that generated passwords had to be secure,
in the end I imposed a minimum length but didn't impose a mininum combination of the different types of characters.

I decided to throw an exception if inputs were invalid, as a returned String error message might have been mistaken
for a password by the caller.

I decided to use StringBuilder since there were no concurrency issues as it was all using local variables so we 
didn't need the thread safety of StringBuffer.

I should have used a resource bundle text file to supply the error messages rather than hard coding them.

I didn't want the passwords to follow a repeating pattern of uppercase, lowercase, number, special, so I used 
StringBuilder.insert(offset, char) with a random offset to put the new character in a random postion in the password string.
This meant an additional check because the first char had to be appended.

I could have written unit tests to ensure the returned password contained all the required inputs rather than printing from main.

