package PasswordGenerator;

import java.util.Random;

public class PasswordGenerator {
	private static final int MINIMUM_SECURE_PASSWORD_LENGTH = 12;
	public String generatePassword(int length, boolean uppercase, boolean lowercase, boolean number, boolean special){
		if(length < MINIMUM_SECURE_PASSWORD_LENGTH){
			throw new IllegalArgumentException("Error: Requested password length is too short");
		}if(!uppercase && !lowercase &&!number &&!special){
			throw new IllegalArgumentException( "Error: Password alphabet too small, must be able to include at least one of uppercase, lowercase, special characters, or numbers");
		}
	
		Random random = new Random();
		StringBuilder password = new StringBuilder();
		while(password.length() < length){
			if(uppercase && password.length() < length){	
				password.insert(random.nextInt(password.length() + 1),generateCharacter(26, 65, random) );
			}
			if(lowercase && password.length() < length){
				password.insert(random.nextInt(password.length() + 1),generateCharacter(26, 97, random) );
			}
			if(number && password.length() < length){
				password.insert(random.nextInt(password.length() + 1),generateCharacter(10, 48, random) );
			}if(special && password.length() < length){
				password.insert(random.nextInt(password.length() + 1),generateCharacter(7, 58, random) );
			}						
		}
		
		return password.toString();
	}
	private Character generateCharacter(int alphabetSize,  int asciiStartIndexOfAlphabet, Random random) {
		return (char) (Math.abs(random.nextInt() % alphabetSize) + asciiStartIndexOfAlphabet);		
	}

	
	 public static void main(String[] args) {
	        PasswordGenerator pg = new PasswordGenerator();
	       System.out.println(pg.generatePassword(15, true, true, false, false)); 
	       System.out.println(pg.generatePassword(15, false, true, false, false)); 
	       System.out.println(pg.generatePassword(15, false, false, true, false)); 
	       System.out.println(pg.generatePassword(50, false, false, false, true)); 
	       
	       System.out.println(pg.generatePassword(100, true, true, true, true)); 
	       
	    }
}
